function [TUcells,TUprop] = shuffleTU(TUcells,TUprop)
% the stage of shuffling will naturally go away, as we will randomly choose
% a certain class object and modify it, without having to keep eye on
% data distributed over many tables
% but unfortunately this whole shuffling effectively suppressess the 
% possibility to use simple shader code ;(

shf = randperm(length(TUcells)); % prepare random shuffling
TUcells = TUcells(shf); % randomly shuffle cells -> cells act in random order

TUprop.isStem = TUprop.isStem(shf); % shuffle stemness property accordingly
TUprop.Pcap = TUprop.Pcap(shf); % shuffle Pmax property accordingly
end