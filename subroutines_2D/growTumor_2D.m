% JN Kather 2017, jakob.kather@nct-heidelberg.de

function [mySystem, finalImage, finalSummary, isImmuneWinIterations, fcount] = ...
    growTumor_2D(mySystem,cnst)
% growTumor_2D performs the actual agent-based modeling in 2D
%       input are two structures: mySystem, defining the initial state of
%       the system; and cnst, defining some global constants

% START PREPARATIONS -------------------------------------------
% throw all model parameters to workspace
cellfun(@(f) evalin('caller',[f ' = mySystem.params.' f ';']), ...
        fieldnames(mySystem.params));
cellfun(@(f) evalin('caller',[f ' = mySystem.grid.' f ';']), ...
        fieldnames(mySystem.grid));
    
if cnst.createNewSystem % create a new (empty) system
    [L, TUcells, IMcells, TUprop, IMprop] = ...
            initializeSystem_2D(N ,M, TUpmax);
        
    Ln = false(size(L));    % initialize necrosis map
    Lf = false(size(L));    % initialize fibrosis map
else % use existing system and grow it further
    cellfun(@(f) evalin('caller',[f ' = mySystem.TU.' f ';']), ...
            fieldnames(mySystem.TU));
    cellfun(@(f) evalin('caller',[f ' = mySystem.IM.' f ';']), ...
            fieldnames(mySystem.IM));
end
% END PREPARATIONS -------------------------------------------
    
% START INITIALIZE AUX VARIABLES  ----------------------------------------
nh = neighborhood_2D(N);    % get neighborhood indices
L = setEdge_2D(L,true);     % set boundary to occupied
rng(initialSeed);           % reset random number generator 
isImmuneWinIterations = 0;  % set immune win flag to 0
fcount = 0;                 % frame counter for video export
finalImage = [];            % set empty resulting image
% END INITIALIZE AUX VARIABLES   -----------------------------------------

% START ITERATION
for i = 1:cnst.nSteps % iterate through time steps

% START TUMOR CELL ROUND ------------------------------------------------
L(Lf) = rand(sum(Lf(:)),1)>stromaPerm; % permeabilize some stroma-filled grid cells 
% does it mean, that peremeability is randomized each time? is it
% reasonable? I'd rather expect the peremability to evolve, but keeping some
% correlation to the previous system permeability

L([IMcells,TUcells]) = true; % ensure that all cells are present on the grid
[TUcells,TUprop] = shuffleTU(TUcells,TUprop);

[L, TUcells, TUprop] = ...
    TU_go_grow_die_2D(L, nh, TUcells, TUprop, ...
                      TUpprol, TUpmig, TUpdeath, TUps);
                  
Lt = updateTumorGrid(L,TUcells); % update tumor grid
% END TUMOR CELL ROUND ---------------------------------------------------

% START MODIFY PARAMETER MAPS --------------------------------------------
[ChtaxMap, HypoxMap] = updateParameterMaps(Lt,Ln,Lf,fillSE,distMaxNecr);
% END MODIFY PARAMETER MAPS

% START IMMUNE CELL ROUND ------------------------------------------------
L([IMcells,TUcells]) = true; % ensure that all cells are present on the grid
    % I wonder why we need this...
if rand()<=IMinfluxProb % randomly trigger influx
    % strange... why doesn't only one variable control immune cells flux?
    % does it mimic immune cell "bursts"?
[L,IMcells,IMprop] = IMinflux(L,IMcells,IMprop,IMpmax,IMkmax,IMinflRate);
end

% TODO: Shuffling here is done only once, and later all cells move in the
% same order
[IMcells,IMprop] = shuffleIM(IMcells,IMprop); % randomly shuffle immune cells

if numel(IMcells)>0 % if there are any immune cells 
    for j = 1:(IMspeed-1) % allow immune cells to move N times per round
        L(Ln) = false; % for immune cell movement, necrosis is invisible
        L(Lf) = rand(sum(Lf(:)),1)>stromaPerm; % permeabilize some stroma-filled grid cells
            % The same argument as in line:44
        L([IMcells,TUcells]) = true; % ensure that all cells are present on the grid
            % The same argument as in line:64
            
        % TODO: It seems, that immune cells, who performed killing, can still
        % move. Check it.
        [L, IMcells] =  IM_go_2D(IMcells, IMpmig, IMrwalk, ChtaxMap, L, nh);

        [TUcells, TUprop, IMcells, IMprop, L, Lt] = ... % tumor cell killing by immune cells 
            IM_kill_TU_2D(TUcells, TUprop, IMcells, IMprop, ...
                          L, Lt, IMpkill, nh, ChtaxMap, engagementDuration);

        IMprop.engaged(IMprop.engaged>0) = IMprop.engaged(IMprop.engaged>0)-1; % un-engage lymphocytes
    end
    
    % allow immune cells to move once more or to proliferate or die
    [L, IMcells, IMprop] = ...
        IM_go_grow_die_2D(IMcells, IMprop, IMpprol, IMpmig, ...
                          IMpdeath, IMrwalk, IMkmax, ChtaxMap, L, nh);
        % It seems like this stage is a shortcut way to keep large part of
        % the code copied from TU_go_grow_die() unchanged. I mean having 
        % an additional step of walking, where all walking may have already
        % be done in IM_go_2D/IM_kill_TU_2D loop doesn't seem natural.
        % TODO IMPORTANT: it also seems that "engaged" cells may proliferate as
        % well. Is it true and/or reasonable? CHECK IT!
              
end % end (if there are any immune cells)

L(Ln|Lf) = true; % fully turn on necrosis and fibrosis again
L([IMcells,TUcells]) = true; % ensure that all cells are present on the grid
% END IMMUNE CELL ROUND --------------------------------------------------

% START NECROSIS  --------------------------------------------
necrNum = sum(rand(numel(TUcells),1) <= probSeedNecr);
    % TODO-check: Here we calculate the number of tumor cells with induced
    % necrosis, but - suspiciously - this is independent of the HypoxMap 
    % while being used later to find necrosis seeds WITH HypoxMap.
if numel(TUcells)>1 && necrNum>0 % seed necrosis
    seedCoords = randsample(TUcells, necrNum, true, HypoxMap(TUcells));
        % TODO-check: Even if we accept the necrosis probability to be independent
        % of the hypoxia map, it seems like sampling here should be 
        % WITHOUT replacement (false)
    necrosisSeeds = false(N,M);
    necrosisSeeds(seedCoords) = true; 
    %disp([num2str(necrNum), ' cell(s) will trigger necrosis']);
    % smooth and expand necrotic seed map
    necrosisSeeds = expandSeedMap(necrosisSeeds, smoothSE, necrFrac);
    seedCoords = find(necrosisSeeds);
    targetIdx = ismember(TUcells, seedCoords); % find indexes of erased tumor cells
    Lt(TUcells(targetIdx)) = false; % remove cell from grid Lt
    Ln(TUcells(targetIdx)) = true; % add to necrosis grid    
    [TUcells,TUprop] = removeTU(TUcells, TUprop, targetIdx); % second, remove from stack
end
% END NECROSIS  ----------------------------------------------

% START FIBROSIS ------------------------------------------------------
fibrosify = ~IMprop.Kcap & (rand(1,numel(IMcells)) < probSeedFibr);
    % Here with necrosis it is reasonable to get fibrosify table using 
    % its killing capacity which turns on only when KCap<1. But calling
    % random number generator more than it is necessary - doesn't look good.
if sum(fibrosify) % exhausted immune cells seed fibrosis
    Lfseed = false(size(L)); % preallocate fibrosis seed map
    Lfseed(IMcells(fibrosify)) = true;
    Lfseed = expandSeedMap(Lfseed,smoothSE,fibrFrac); % smooth and expand fibrotic seed map
    Lfseed(TUcells) = false; 
    Lf(Lfseed & ~Ln) = true; % update fibrosis grid
    [IMcells,IMprop] = removeIM(IMcells,IMprop,fibrosify); % remove fibrosifying immune cells
    L(Lf) = true; % update L grid (master grid)
end
% END FIBROSIS ----------------------------------------------------------

% START DRAWING ---------------------------------------------------------
tumorIsGone = (sum(Lt(:))==0);
if (mod(i-1,cnst.drawWhen)==cnst.drawWhen-1) || tumorIsGone % plot status after N epochs 
    fcount = fcount+1;
    disp(['finished iteration ',num2str(i)]);
    % export current state of the system
    [mySystem,currentSummary] = updateSystem(mySystem,TUcells,TUprop,...
        IMcells,IMprop,ChtaxMap,HypoxMap,L,Lt,Ln,Lf,i,cnst);
    if cnst.verbose % enforce drawing and create image from plot
    visualize_balls_2D_blank(mySystem);
    drawnow, currFrame = getframe(gca);
    finalImage{fcount} = currFrame.cdata;
    finalSummary{fcount} = currentSummary;
    else finalImage = []; finalSummary = [];
    end
end
% END DRAWING -----------------------------------------------------------

% if there are no tumor cells anymore then the game is over
if tumorIsGone
    disp('Immune cells win');
    isImmuneWinIterations = i;
    return
end
if debugmode && findInconsistency(Lt,Lf,Ln,TUcells,IMcells,TUprop,IMprop)  
     error('SEVERE ERROR: INCONSISTENCY FOUND');
end % END DEBUG
end % END ITERATION
end % END FUNCTION
        