function [P,D,Mi] = CellWhichAction(randFloats, pprol, pdeath, pmig)
% should check if sum <= 1.0?
P = randFloats <= pprol; % GROW: proliferation
D = (randFloats <= (pdeath+pprol)) & (randFloats > pprol); % DIE: spontaneous death can happen anytime
Mi = (randFloats <= (pdeath+pprol+pmig)) &  (randFloats > (pdeath+pprol)); % GO: migrate if no grow and no die
end