function nh = neighborhood_2D(N)
    nh.aux = int32([-N-1 -N -N+1 -1 1 N-1 N N+1])'; % indices to neighborhood
    nh.permutations = perms(uint8(1:8))'; % permutations of adjacent positions
    nh.noOfPermutations = size(nh.permutations,2); % number of possible permutations
end
