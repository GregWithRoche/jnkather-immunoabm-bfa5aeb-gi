function seedmap = expandSeedMap(seedmap,smoothSE,frac)

    sngh = smoothSE.Neighborhood;
    sngh(rand(size(sngh))>frac) = 0;
    sngh = imclose(sngh,ones(5)); % fill holes in mask
        % TODO: why ones(5) is used here? e.g. why not ones(4)? not documented
    seedmap = imdilate(seedmap,sngh);
    
    % Generally - this seems a very arbitrary way to generate necrosis
    % and/or fibrosis maps. How this is procedure grounded in science?
end