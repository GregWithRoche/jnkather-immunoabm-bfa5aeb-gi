% JN Kather 2017 (jakob.kather@nct-heidelberg.de)
% inspired by Jan Poleszczuk
% this function can be compiled with the MATLAB code generator

function [L, IMcells, IMprop] = ...
    IM_go_grow_die_2D(IMcells, IMprop, IMpprol, IMpmig, ...
                      IMpdeath, IMrwalk, IMkmax, ChtaxMap, L, nh)
    
adjacentSurrounding = getAdjacentSurrounding_2D(L, IMcells, nh); % create masks for adjacent positions

% P, D and Mi are mutually exclusive; Ps and De are dependent on P
[Proliferating, Dying, Migrating] = ...
    CellWhichAction(adjacentSurrounding.randFloatsForFreeSpotAgents, ... 
                    IMpprol, IMpdeath, IMpmig);
                
Exhausted = Proliferating & (IMprop.Pcap(adjacentSurrounding.indxsFreeSpot) == 0); % proliferation capacity exhaustion -> Die
deleted = Dying | Exhausted; % cells to delete
actingCellsIndxs = find((Proliferating | Migrating) & ~deleted); % indices to the cells that will perform action

for iloop = 1:numel(actingCellsIndxs) % only for those that will do anything
    currActingIdx = actingCellsIndxs(iloop); % number within stack of currently acting cell
    ngh = adjacentSurrounding.indexes(:,adjacentSurrounding.indxsFreeSpot(currActingIdx)); % cells neighborhood
    ngh2 = ngh(ngh>0); % erasing places that were previously occupied
    indOL = find(~L(ngh2)); %selecting all free spots
        % this different comparing to TU_go_grow_die()
    chemo = ChtaxMap(ngh2(:)); % extract chemotaxis value at neighbors
        % here again code copying starts, as chemotaxis phase is already
        % implemented in IM_go_2D()
    chemo = chemo(~L(ngh2)); % block occupied spots   
    if ~isempty(chemo) % use spot with highest chemo value
        chemo = chemo/max(chemo(:)); % normalize
        chemo = (1-IMrwalk) * chemo + IMrwalk * rand(size(chemo));
        [~,cid] = min(chemo); % lowest distance 
        indO = indOL(cid(1));   
        if ~isempty(indO) %if there is still a free spot
            L(ngh2(indO)) = true; % add new/move cell to grid
            if Proliferating(currActingIdx) % proliferation
                IMcells = [IMcells uint32(ngh2(indO))]; % add new cell to stack
                IMprop.Pcap(adjacentSurrounding.indxsFreeSpot(currActingIdx)) = IMprop.Pcap(adjacentSurrounding.indxsFreeSpot(currActingIdx))-1; % decrease remaining prol. cap.
                IMprop.Pcap = [IMprop.Pcap, IMprop.Pcap(adjacentSurrounding.indxsFreeSpot(currActingIdx))]; % update property vector for Pmax
                IMprop.Kcap = [IMprop.Kcap, IMkmax]; % update property vector for remaining kills
                IMprop.engaged = [IMprop.engaged, 0]; % update property vector for engagement
            else % migration
                L(IMcells(adjacentSurrounding.indxsFreeSpot(currActingIdx))) = false; %freeing spot
                IMcells(adjacentSurrounding.indxsFreeSpot(currActingIdx)) = uint32(ngh2(indO));
            end
        end
    end
end

if ~isempty(deleted) % updating immune cell death
    L(IMcells(adjacentSurrounding.indxsFreeSpot(deleted))) = false;     % remove immune cell from grid
    [IMcells,IMprop] = removeIM(IMcells,IMprop,adjacentSurrounding.indxsFreeSpot(deleted)); % second, remove from stack
end
 
end
