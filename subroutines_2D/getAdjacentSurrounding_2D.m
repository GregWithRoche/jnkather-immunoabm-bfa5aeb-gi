function surr = getAdjacentSurrounding_2D(L, MYcells, nh)

randomPermutationIndexesForEachCell = randi(nh.noOfPermutations, 1, length(MYcells));
surr.indexes = bsxfun(@plus, MYcells, ...
                        nh.aux(nh.permutations(:,randomPermutationIndexesForEachCell)));
    % this gives a vector of shuffled adjacent indexes for each cell
surr.indexes(L(surr.indexes)) = 0; % setting occupied adjacent grid positions to false
    % table of vectors of adjacent cell indexes, where occupied are set  to 0
surr.indxsFreeSpot = find(any(surr.indexes)); % selecting agents with at least one free spot
    % indexes in the table of vectors, which have a free spot
surr.freeSpotAgentNumber = length(surr.indxsFreeSpot); % number of agents with free spot
surr.randFloatsForFreeSpotAgents = rand(1, surr.freeSpotAgentNumber); % initialize random number vector
    % each agent having a free-spot will have a random number assigned
end