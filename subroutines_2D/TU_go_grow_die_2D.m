% JN Kather 2017 (jakob.kather@nct-heidelberg.de)
% inspired by Jan Poleszczuk
% this function can be compiled with the MATLAB code generator

function [L, TUcells, TUprop] = ...
    TU_go_grow_die_2D(L, nh, TUcells, TUprop, TUpprol, ...
                      TUpmig, TUpdeath, TUps)

try
    adjacentSurrounding = getAdjacentSurrounding_2D(L, TUcells, nh); % create masks for adjacent positions
catch
    warning('severe error: could not get neighborhood.');
    whos, disp(char(10));
    save SEVERE_ERROR; pause(60);
    warning('error log saved. will continue...');
end

% P, D and Mi are mutually exclusive; Ps and De are dependent on P
[Proliferating, Dying, Migrating] = ...
    CellWhichAction(adjacentSurrounding.randFloatsForFreeSpotAgents, ...
                    TUpprol, TUpdeath, TUpmig);
    % here logical 0/1 values in [Proliferating, Dying, Migrating] tables 
    % refer to cell indexes for cells with free spots in adjacentSurrounding
        % IMPORTANT: is this a mistake? it seems like only tumor cels with
        % free spots around can actually die
    
ProlifSymmetric = Proliferating & ...
                  rand(1, adjacentSurrounding.freeSpotAgentNumber) <= TUps & ...
                  TUprop.isStem(adjacentSurrounding.indxsFreeSpot); % symmetric division
                  
Exhausted = Proliferating & (TUprop.Pcap(adjacentSurrounding.indxsFreeSpot) == 0); % proliferation capacity exhaustion -> Die
deleted = Dying | Exhausted; % find dead / dying cells
actingCellsIndxs = find((Proliferating | Migrating) & ~deleted); % live cells that will proliferate or migrate
    % indexes of a subset od free spot agents in adjacentSurrounding 

for iloop = 1:numel(actingCellsIndxs) % only for those that will do anything
    currActingIdx = actingCellsIndxs(iloop); % number within stack of currently acting cells
    ngh = adjacentSurrounding.indexes(:,adjacentSurrounding.indxsFreeSpot(currActingIdx)); % cells neighborhood
    ngh2 = ngh(ngh>0); % erasing places occupied during getAdjacentSurrounding_2D call
        % this is NOT because cells may migrate from spot to spot, or die while 
        % this loop is running - may we change it?
    indO = find(~L(ngh2),1,'first'); % selecting free spot
        % this index points at the neighbourhood (1:8)
        % Here it may happen, that in previous loop runs, the free cell
        % adjacent grid positions became occupied. This effectively makes
        % the use of GPU code trickier. But it seems that such situations
        % are rather a second-order processes and may probably be
        % neglected until the method matures much more (or we may simulate
        % on GPU consecutive chains of non-interfering events).
    if ~isempty(indO) % if there is still a free spot
        L(ngh2(indO)) = true; % add new/move cell to grid
        if Proliferating(currActingIdx) % proliferation happens
            newCell = uint32(ngh2(indO)); % find place for new cell
            TUcells = [TUcells, newCell(1)]; % add new cell to stack
                % MatLab IDE advices to prealocate TUcells here - maybe
                % worth considering?
            if ProlifSymmetric(currActingIdx) % symmetric division
               TUprop.isStem = [TUprop.isStem, true];
               TUprop.Pcap = [TUprop.Pcap, ...
                              TUprop.Pcap(adjacentSurrounding.indxsFreeSpot(currActingIdx))];  
            else % asymmetric division
               TUprop.isStem = [TUprop.isStem, false];
               TUprop.Pcap = [TUprop.Pcap, TUprop.Pcap(adjacentSurrounding.indxsFreeSpot(currActingIdx))-1];
               if ~TUprop.isStem(adjacentSurrounding.indxsFreeSpot(currActingIdx)) % reduce proliferation capacity
                TUprop.Pcap(adjacentSurrounding.indxsFreeSpot(currActingIdx)) = TUprop.Pcap(adjacentSurrounding.indxsFreeSpot(currActingIdx))-1;
               end
            end
        else % migration
            L(TUcells(adjacentSurrounding.indxsFreeSpot(currActingIdx))) = false; % freeing spot
            TUcells(adjacentSurrounding.indxsFreeSpot(currActingIdx)) = uint32(ngh2(indO)); % update cell position
        end
    end
end

if ~isempty(deleted) % remove dead tumor cells
    L(TUcells(adjacentSurrounding.indxsFreeSpot(deleted))) = false;      % first, remove from grid
    [TUcells,TUprop] = removeTU(TUcells,TUprop,adjacentSurrounding.indxsFreeSpot(deleted)); % second, remove from stack
end 
end
